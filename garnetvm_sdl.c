#include "garnetvm.h"
#include <SDL2/SDL.h>
#include <stdlib.h>
#include <stdint.h>
#include <assert.h>

SDL_Window *window;
SDL_Renderer *renderer;
char mdfdn = 0;

unsigned char palette[] = {
    0x00,0x00,0x00,0xff,
    0x00,0x00,0x80,0xff,
    0x00,0x80,0x00,0xff,
    0x00,0x80,0x80,0xff,
    0x80,0x00,0x00,0xff,
    0x80,0x00,0x80,0xff,
    0x80,0x80,0x00,0xff,
    0x80,0x80,0x80,0xff,
    0x40,0x40,0x40,0xff,
    0x40,0x40,0xff,0xff,
    0x40,0xff,0x40,0xff,
    0x40,0xff,0xff,0xff,
    0xff,0x40,0x40,0xff,
    0xff,0x40,0xff,0xff,
    0xff,0xff,0x40,0xff,
    0xff,0xff,0xff,0xff,
};

void endSDL() {
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

void update() {
    SDL_Event ev;
    int i, ts, c1, c2, y, e, t, j, x1, y1, s;
    SDL_Rect r;

    while(SDL_PollEvent(&ev)) {
        switch(ev.type) {
        case SDL_QUIT: endSDL(); exit(1);
        case SDL_TEXTINPUT:
            memory[2] = *ev.text.text;
            break;
        case SDL_KEYDOWN:
            switch(ev.key.keysym.sym) {
            case SDLK_BACKSPACE: memory[2] = 127; break;
            case SDLK_RETURN: memory[2] = 13; break;
            case SDLK_TAB: memory[2] = (mdfdn&2) ? 10 : 9; break;
            case SDLK_UP: memory[2] = 16; break;
            case SDLK_DOWN: memory[2] = 14; break;
            case SDLK_LEFT: memory[2] = 2; break;
            case SDLK_RIGHT: memory[2] = 6; break;
            case SDLK_ESCAPE: memory[2] = 27; break;
            case SDLK_LCTRL: case SDLK_RCTRL: mdfdn |= 1; break;
            case SDLK_LSHIFT: case SDLK_RSHIFT: mdfdn |= 2; break;
            default:
                if(ev.key.keysym.sym >= 'a' && ev.key.keysym.sym <= 'z'
                   && mdfdn&1)
                    memory[2] = ev.key.keysym.sym - 'a' + 1;
                break;
            }
            break;
        case SDL_KEYUP:
            switch(ev.key.keysym.sym) {
            case SDLK_LCTRL: case SDLK_RCTRL: mdfdn ^= 1; break;
            case SDLK_LSHIFT: case SDLK_RSHIFT: mdfdn ^= 2; break;
            }
            break;
        }
    }

    /* draw */

    SDL_GetWindowSize(window, &i, &j);
    x1 = i/320;
    y1 = j/200;
    if(x1 > y1) s = y1;
    else s = x1;
    if(s < 1) s = 1;
    x1 = i/2-320*s/2;
    y1 = j/2-200*s/2;

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0xff);
    SDL_RenderClear(renderer);

    ts = *(uint16_t*)&memory[0];
    r.w = s;
    r.h = s;

    for(i = 0; i < 40*25; i++) {
        c1 = memory[1003+i]&0xf;
        c2 = memory[1003+i]>>4;
        t = memory[3+i];

        for(y = 0; y < 8; y++) {
            e = memory[ts+t*8+y];
            for(j = 0; j < 8; j++) {
                if(e&(1<<j))
                    SDL_SetRenderDrawColor(renderer,
                        palette[c1*4], palette[c1*4+1], palette[c1*4+2],
                        0xff);
                else
                    SDL_SetRenderDrawColor(renderer,
                        palette[c2*4], palette[c2*4+1], palette[c2*4+2],
                        0xff);

                r.x = (i%40)*8*s+x1+j*s;
                r.y = (i/40)*8*s+y1+y*s;
                SDL_RenderFillRect(renderer, &r);
            }
        }
    }

    SDL_RenderPresent(renderer);
}

int main(int argc, char **args) {
    assert(SDL_Init(SDL_INIT_EVERYTHING) >= 0);
    assert(window = SDL_CreateWindow(args[1],
           SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
           640, 480, SDL_WINDOW_RESIZABLE));
    assert(renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_SOFTWARE));

    garnetMain(argc, args);

    endSDL();
    return 0;
}
