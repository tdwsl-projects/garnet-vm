; graphics/tiles test

  *0x1000

  /syscalls.inc

  l.w $0 #font
  sw.w $0 #mem_tiles

  xor $0 $0
  l.b $1 0x4f
  xor $8 $8

:draw
  l.w $3 #mem_sram
  l.w $4 #mem_cram
  l.w $5 #1000
:draw_0
  sb $0 $3
  sb $1 $4
  inc $0

:draw_1
  inc $1

  and.w $6 $1 #0x000f
  and.w $7 $1 #0x00ff
  shr $7 $4
  beq $7 $6 @draw_1

  inc $3
  inc $4
  dec $5
  bnz $5 @draw_0

  :mem_sram+1 =mem_sram +1

  l $9 $1
  l $0 $8
  shr $0 $4
  jal #nib
  sb.w $0 #mem_sram
  l $0 $8
  jal #nib
  sb.w $0 #mem_sram+1
  l.w $0 #0x9e9e
  sw.w $0 #mem_cram
  l $1 $9

:forever
  lb.w $8 #mem_key
  bz $8 @forever
  xor $0 $0
  sb.w $0 #mem_key
  j #draw

:nib
  and.w $0 #0x000f
  l.b $1 10
  blt $0 $1 @nib_0

  add.b $0 87 ; a = 97
  jr $31

:nib_0
  add.b $0 "0
  jr $31

:font %font0.bin
+-0x100
