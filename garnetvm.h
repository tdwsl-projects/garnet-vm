#ifndef GARNETVM_H
#define GARNETVM_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#define MEMORY_SIZE 65536*8
#define ENTRY_POINT 0x1000
#define NFILES 16
#define UPDATEMASK 0x3fff

enum {
    SYS=0, JR, L, LW, LB, SW, SB, ADD,
    SUB, AND, OR, XOR, INC, DEC, SHR, SHL,
    MUL, DIV, IB, OB, SHR_R, SHL_R, PH, PL,
    L_W=0x20, JALR, ADD_W, AND_W, OR_W, XOR_W, LW_W, LB_W,
    SW_W, SB_W,
    L_B=0x30, ADD_B, BZ=0x34, BNZ, BM, BNM,
    BEQ, BNE, BLT, BLE, JAL=0x3e, J=0x3f,
};

const char *insStrs[] = {
    "sys", "jr", "l", "lw", "lb", "sw", "sb", "add",
    "sub", "and", "or", "xor", "inc", "dec", "shr", "shl",
    "mul", "div", "shr.r", "shl.r", "ph", "pl", 0,0, 0,0,0,0,0,0,0,0,
    "l.w", "jalr", "add.w", "and.w", "or.w", "xor.w", "lw.w", "lb.w",
    "sw.w", "sb.w", 0,0,0,0,0,0,
    "l.b", "add.b", 0,0, "bz", "bnz", "bm", "bnm",
    "beq", "bne", "blt", "ble", 0,0, "jal","j",
};

FILE *files[NFILES] = {0};
unsigned char memory[MEMORY_SIZE];
int16_t regs[32];
uint16_t pc = ENTRY_POINT;
unsigned int ibank = 0, obank = 0;
char **g_args;
int g_argc;

int trace = 0;

void printIns() {
    int ins, ra, rb, a, b;

    ins = memory[pc]&0x3f;
    ra = (memory[pc]&0xc0)>>3|memory[pc+1]>>5;
    rb = memory[pc+1]&0x1f;

    switch(ins&0xf0) {
    case 0x00:
    case 0x10:
        a = memory[pc+1];
        break;
    case 0x20:
        a = *(int16_t*)&memory[pc+2];
        break;
    case 0x30:
        a = (char)memory[pc+2];
        b = *(int16_t*)&memory[pc+1];
        break;
    }

    printf("%.4X %s ", pc, insStrs[ins]);

    switch(ins) {
    case SYS: case IB: case OB:
        printf("%.2X", a&0xff); break;
    case JR: case INC: case DEC:
        printf("$%d", ra); break;
    case L: case LW: case LB: case SW: case SB: case ADD:
    case SUB: case AND: case OR: case XOR:
    case MUL: case DIV: case SHR_R: case SHL_R:
    case PH: case PL:
        printf("$%d $%d", ra, rb); break;
    case L_W: case JALR: case ADD_W: case AND_W: case OR_W: case XOR_W:
    case LW_W: case LB_W: case SW_W: case SB_W:
        printf("$%d $%d %.4X", ra, rb, a); break;
    case L_B: case ADD_B:
        printf("$%d $%d %.2X", ra, rb, a&0xff); break;
    case BZ: case BNZ: case BM: case BNM:
        printf("$%d %.4X", ra, pc+3+a); break;
    case BEQ: case BNE: case BLT: case BLE:
        printf("$%d $%d %.4X", ra, rb, pc+3+a); break;
    case JAL: case J:
        printf("%.4X", b); break;
    }

    printf("\n");
}

void printRegs() {
    int i;
    printf("     ");
    for(i = 0; i < 32; i++) {
        printf("%2d=%.4X ", i, regs[i]&0xffff);
        if(((i+1)%8) == 0) printf("\n     ");
    }
    printf("\n");
}

void update();

void run() {
    int ins, ra, rb, a, b, i, uc;

    uc = 0;

    for(;;) {
        if(trace) { printRegs(); printIns(); }

        ins = memory[pc]&0x3f;
        ra = (memory[pc]&0xc0)>>3|memory[pc+1]>>5;
        rb = memory[pc+1]&0x1f;

        switch(ins&0xf0) {
        case 0x00:
        case 0x10:
            a = memory[pc+1];
            pc += 2;
            break;
        case 0x20:
            a = *(int16_t*)&memory[pc+2];
            pc += 4;
            break;
        case 0x30:
            a = (char)memory[pc+2];
            b = *(int16_t*)&memory[pc+1];
            pc += 3;
            break;
        }

        switch(ins) {
        case SYS:
            /* syscall */
            switch(a) {
            case 0: return;
            case 1: fputc(regs[0], stdout); break;
            case 2: update(); regs[0] = fgetc(stdin); break;
            case 3:
                for(i = 0; i < NFILES && files[i]; i++);
                if(i >= NFILES) { regs[0] = 0; break; }
                if(regs[1] == 0)
                    files[i] = fopen((char*)memory+regs[0], "rb");
                else if(regs[1] == 1)
                    files[i] = fopen((char*)memory+regs[0], "wb");
                if(files[i])
                    regs[0] = i+1;
                else
                    regs[0] = 0;
                break;
            case 4:
                if(regs[0]-1 < 0 || regs[0]-1 >= NFILES) break;
                fclose(files[regs[0]-1]);
                break;
            case 5:
                if(regs[0]-1 < 0 || regs[0]-1 >= NFILES) break;
                fputc(regs[1], files[regs[0]-1]);
                break;
            case 6:
                if(regs[0]-1 < 0 || regs[0]-1 >= NFILES) { regs[1] = 0; break; }
                regs[1] = fgetc(files[regs[0]-1]);
                break;
            case 7:
                if(regs[0]-1 < 0 || regs[0]-1 >= NFILES) {
                    regs[1] = -1;
                    break;
                }
                if(!files[regs[0]-1]) { regs[1] = -1; break; }
                regs[1] = feof(files[regs[0]-1]) ? -1 : 0;
                break;

            case 0x10:
                regs[0] = g_argc;
                break;
            case 0x11:
                strncpy(&memory[regs[1]], g_args[regs[0]], regs[2]);
                if(strlen(g_args[regs[0]]) >= regs[2])
                    memory[regs[1]+regs[2]] = 0;
                break;

            default:
                printf("unknown syscall %.2X at %.4X\n", pc-2);
                break;
            }

            break;

        case JR: pc = regs[ra]; break;
        case L: regs[ra] = regs[rb]; break;
        case LW: regs[ra] = *(int16_t*)&memory[ibank|regs[rb]]; break;
        case LB: regs[ra] = memory[ibank|regs[rb]]; break;
        case SW: *(int16_t*)&memory[obank|regs[rb]] = regs[ra]; break;
        case SB: memory[obank|regs[rb]] = regs[ra]; break;
        case ADD: regs[ra] += regs[rb]; break;
        case SUB: regs[ra] -= regs[rb]; break;
        case AND: regs[ra] &= regs[rb]; break;
        case OR: regs[ra] |= regs[rb]; break;
        case XOR: regs[ra] ^= regs[rb]; break;
        case INC: regs[ra]++; break;
        case DEC: regs[ra]--; break;
        case SHR: regs[ra] >>= rb; break;
        case SHL: regs[ra] <<= rb; break;
        case DIV: a = regs[ra]; b = regs[rb];
            regs[ra] = a/b; regs[rb] = a%b; break;
        case MUL: regs[ra] *= regs[rb]; break;
        case IB: ibank = a<<16; break;
        case OB: obank = a<<16; break;
        case SHR_R: regs[ra] >>= regs[rb]; break;
        case SHL_R: regs[ra] <<= regs[rb]; break;
        case PH: regs[rb] -= 2;
            *(int16_t*)&memory[obank|regs[rb]] = regs[ra]; break;
        case PL: regs[ra] = *(int16_t*)&memory[ibank|regs[rb]];
            regs[rb] += 2; break;

        case L_W: regs[ra] = a; break;
        case JALR: regs[ra] = pc; pc = a; break;
        case ADD_W: regs[ra] = regs[rb] + a; break;
        case AND_W: regs[ra] = regs[rb] & a; break;
        case OR_W: regs[ra] = regs[rb] | a; break;
        case XOR_W: regs[ra] = regs[rb] ^ a; break;
        case LW_W: regs[ra] = *(int16_t*)&memory[ibank|a]; break;
        case LB_W: regs[ra] = memory[ibank|a]; break;
        case SW_W: *(int16_t*)&memory[obank|a] = regs[ra]; break;
        case SB_W: memory[obank|a] = regs[ra]; break;

        case L_B: regs[ra] = a; break;
        case ADD_B: regs[ra] = regs[rb] + a; break;
        case BZ: if(!regs[ra]) pc += a; break;
        case BNZ: if(regs[ra]) pc += a; break;
        case BM: if(regs[ra]<0) pc += a; break;
        case BNM: if(regs[ra]>=0) pc += a; break;
        case BEQ: if(regs[ra]==regs[rb]) pc += a; break;
        case BNE: if(regs[ra]!=regs[rb]) pc += a; break;
        case BLT: if(regs[ra]< regs[rb]) pc += a; break;
        case BLE: if(regs[ra]<=regs[rb]) pc += a; break;

        case JAL: regs[31] = pc; pc = b; break;
        case J: pc = b; break;

        default:
            switch(ins&0xf0) {
            case 0x00:
            case 0x10:
                pc -= 2;
                break;
            case 0x20:
                pc -= 4;
                break;
            case 0x30:
                pc -= 3;
                break;
            }

            printf("unknown instruction %.2X at %.4X\n", ins, pc);
            exit(1);
            break;
        }

        uc++;
        uc &= UPDATEMASK;
        if(!uc) update();
    }
}

void loadFile(const char *filename) {
    FILE *fp;
    fp = fopen(filename, "rb");
    if(!fp) {
        printf("failed to open %s\n", filename);
        exit(1);
    }
    fread(memory+ENTRY_POINT, 1, MEMORY_SIZE-ENTRY_POINT, fp);
    fclose(fp);
    pc = ENTRY_POINT;
}

int garnetMain(int argc, char **args) {
    if(argc > 2) {
        if(!strcmp(args[argc-1], "-t")) {
            trace = 1;
            argc--;
        }
    }
    if(argc < 2) {
        printf("usage: %s <file>\n", args[0]);
        return 0;
    }
    g_args = args+1;
    g_argc = argc-1;
    loadFile(args[1]);
    run();
    return 0;
}

#endif
