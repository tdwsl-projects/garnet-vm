; 99 bottles of beer on the wall

  *0x1000

  /syscalls.inc

  l.w $30 #0x1000

  l.b $10 99
:l0
  l.b $0 32
  sys sys_putc
  sys sys_putc

  jal #putbeer
  l.w $1 #beer2
  jal #puts
  l.b $0 ",
  sys sys_putc
  l.b $0 10
  sys sys_putc

  jal #putbeer
  l.w $1 #beer3
  jal #puts

  l.w $1 #beer4
  jal #puts

  dec $10

  jal #putbeer
  l.w $1 #beer2
  jal #puts
  l.b $0 ".
  sys sys_putc
  l.b $0 10
  sys sys_putc

  l.b $0 10
  sys sys_putc

  bnz $10 @l0

  sys sys_quit

:putbeer
  bz $10 @putbeer_1

  l $1 $10
  ph $31 $30
  jal #putn
  pl $31 $30
  l.b $0 1
  bne $10 $0 @putbeer_0

  l.w $1 #beer0
  j #puts

:putbeer_0
  l.w $1 #beer1
  j #puts

:putbeer_1
  l.b $0 "n
  sys sys_putc
  l.b $0 "o
  sys sys_putc
  j #putbeer_0

:putn
  l.b $2 10
  l.w $3 #nbuf

:putn_0
  l $0 $2
  div $1 $0
  sb $0 $3
  inc $3
  bnz $1 @putn_0

  l.w $1 #nbuf
:putn_1
  dec $3
  lb $0 $3
  add.b $0 "0
  sys sys_putc
  bne $1 $3 @putn_1

  jr $31

:puts
  lb $0 $1
  bz $0 @puts_0
  sys sys_putc
  inc $1
  j #puts
:puts_0
  jr $31

:beer0 32 "bottle 0
:beer1 32 "bottles 0
:beer2 32 "of 32 "beer 32 "on 32 "the 32 "wall 0
:beer3 32 "of 32 "beer~ 10 0
:beer4 "take 32 "one 32 "down, 10 "pass 32 "it 32 "around, 10 0

:nbuf
