; hello world

  /syscalls.inc

  *0x1000

  l.w $1 #hellomsg
  jal #puts

  l.w $1 #512
  jal #putn
  l.b $0 10
  sys sys_putc

  l.w $1 #-72
  jal #putn
  l.b $0 10
  sys sys_putc

  sys sys_quit

; print a signed integer

:putn
  l.w $2 #nbuf_end
  l $3 $2
  bnm $1 @putn_0

  xor.w $1 #0xffff
  inc $1
  l.b $0 "-
  sys sys_putc

:putn_0
  l.b $0 10
  div $1 $0
  dec $2
  sb $0 $2
  bnz $1 @putn_0

:putn_1
  lb $0 $2
  inc $2
  add.b $0 "0
  sys sys_putc
  bne $2 $3 @putn_1

  jr $31

; print a string

:puts
  lb $0 $1
  bz $0 @puts_0
  sys sys_putc
  inc $1
  j #puts
:puts_0
  jr $31

:hellomsg
  "Hello, 32 "world! 10 0

:nbuf
:nbuf_end =nbuf +40
