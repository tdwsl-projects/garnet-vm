
const SYS=0, JR=1, L=2, LW=3, LB=4, SW=5, SB=6, ADD=7,
  SUB=8, AND=9, OR=10, XOR=11, INC=12, DEC=13, SHR=14, SHL=15,
  MUL=16, DIV=17, IB=18, OB=19, SHR_R=20, SHL_R=21, PH=22, PL=23,
  L_W=32, JALR=33, ADD_W=34, AND_W=35, OR_W=36, XOR_W=37, LW_W=38, LB_W=39,
  SW_W=40, SB_W=41,
  L_B=48, ADD_B=49, BZ=52, BNZ=53, BM=54, BNM=55,
  BEQ=56, BNE=57, BLT=58, BLE=59, JAL=62, J=63;

const insStrs = [
    "sys", "jr", "l", "lw", "lb", "sw", "sb", "add",
    "sub", "and", "or", "xor", "inc", "dec", "shr", "shl",
    "mul", "div", "shr.r", "shl.r", "ph", "pl", 0,0, 0,0,0,0,0,0,0,0,
    "l.w", "jalr", "add.w", "and.w", "or.w", "xor.w", "lw.w", "lb.w",
    "sw.w", "sb.w", 0,0,0,0,0,0,
    "l.b", "add.b", 0,0, "bz", "bnz", "bm", "bnm",
    "beq", "bne", "blt", "ble", 0,0, "jal","j",
];

const palette = [
    "#000000", "#000080", "#008000", "#008080",
    "#800000", "#800080", "#808000", "#808080",
    "#404040", "#4040ff", "#40ff40", "#40ffff",
    "#ff4040", "#ff40ff", "#ffff40", "#ffffff",
];

let regs = new Int16Array(32);
let memory = new Uint8Array(65536*8);
const ENTRY_POINT = 0x1000;
let pc = ENTRY_POINT;
let ibank=0, obank=0;

let trace = false;
let logStr = "";

const cvs = document.getElementById("canvas");
const ctx = cvs.getContext("2d");

function getw(a) {
    return memory[a]|memory[a+1]<<8;
}

function setw(a, w) {
    memory[a] = w&0xff;
    memory[a+1] = (w&0xffff)>>8;
}

function log(s) {
    logStr += s;
    let l = logStr.split("\n");
    for(let i = 0; i < l.length-1; i++)
        console.log(l[i]);
    logStr = l[l.length-1];
}

function flushLog() {
    if(logStr == "") return;
    console.log(logStr);
    logStr = "";
}

function signb(b) {
    if(b&0x80) return (b^0xff)*-1-1;
    return b;
}

function printIns() {
    let ins, ra, rb, a, b;

    ins = memory[pc]&0x3f;
    ra = (memory[pc]&0xc0)>>3|memory[pc+1]>>5;
    rb = memory[pc+1]&0x1f;

    switch(ins&0xf0) {
    case 0x00:
    case 0x10:
        a = memory[pc+1];
        break;
    case 0x20:
        a = getw(pc+2);
        break;
    case 0x30:
        a = signb(memory[pc+2]);
        b = getw(pc+1);
        break;
    }

    flushLog();

    log(pc+" "+insStrs[ins]+" ");

    switch(ins) {
    case SYS: case IB: case OB:
        log(a&0xff); break;
    case JR: case INC: case DEC:
        log("$"+ra); break;
    case L: case LW: case LB: case SW: case SB: case ADD:
    case SUB: case AND: case OR: case XOR:
    case DIV: case MUL:
        log("$"+ra+" $"+rb); break;
    case L_W: case JALR: case LW_W: case LB_W: case SW_W: case SB_W:
        log("$"+ra+" $"+rb+" "+a); break;
    case L_B: case ADD_B:
        log("$"+ra+" $"+rb+" "+a&0xff); break;
    case BZ: case BNZ: case BM: case BNM:
        log("$"+ra+" "+pc+3+a); break;
    case BEQ: case BNE: case BLT: case BLE:
        log("$"+ra+" $"+rb+" "+pc+3+a); break;
    case JAL: case J:
        log(b); break;
    }

    flushLog();
}

function printRegs() {
    flushLog();
    for(let i = 0; i < 32; i++)
        log(regs[i]+" ");
    flushLog();
}

function steps() {
    let ins, ra, rb, a, b, i;

    let n = 99999;

    while(n--) {
        if(trace) { printRegs(); printIns(); }

        ins = memory[pc]&0x3f;
        ra = (memory[pc]&0xc0)>>3|memory[pc+1]>>5;
        rb = memory[pc+1]&0x1f;

        switch(ins&0xf0) {
        case 0x00:
        case 0x10:
            a = memory[pc+1];
            pc += 2;
            break;
        case 0x20:
            a = getw(pc+2);
            pc += 4;
            break;
        case 0x30:
            a = signb(memory[pc+2]);
            b = getw(pc+1);
            pc += 3;
            break;
        }

        switch(ins) {
        case SYS:
            // syscall
            switch(a) {
            case 0: flushLog(); clearInterval(stepInterval);
                clearInterval(drawInterval); return;
            case 1: log(String.fromCharCode(regs[0])); break;
            case 2: flushLog(); regs[0] = 0xffff; break;
            case 3: regs[1] = 0xffff; break;
            case 4: break;
            case 5: break;
            case 6: break;
            case 7: regs[1] = 0xffff; break;

            case 0x10: regs[0] = 0; break;
            case 0x11: break;

            default:
                flushLog();
                console.log("unknown syscall "+a+" at "+pc);
                break;
            }
            break;

        case JR: pc = regs[ra]; break;
        case L: regs[ra] = regs[rb]; break;
        case LW: regs[ra] = getw(ibank|regs[rb]); break;
        case LB: regs[ra] = memory[ibank|regs[rb]]; break;
        case SW: setw(obank|regs[rb], regs[ra]); break;
        case SB: memory[obank|regs[rb]] = regs[ra]; break;
        case ADD: regs[ra] += regs[rb]; break;
        case SUB: regs[ra] -= regs[rb]; break;
        case AND: regs[ra] &= regs[rb]; break;
        case OR: regs[ra] |= regs[rb]; break;
        case XOR: regs[ra] ^= regs[rb]; break;
        case INC: regs[ra]++; break;
        case DEC: regs[ra]--; break;
        case SHR: regs[ra] >>= rb; break;
        case SHL: regs[ra] <<= rb; break;
        case DIV: a = regs[ra]; b = regs[rb];
            regs[ra] = Math.floor(a/b); regs[rb] = (a%b); break;
        case MUL: regs[ra] *= regs[rb]; break;
        case IB: ibank = a<<16; break;
        case OB: obank = a<<16; break;
        case SHR_R: regs[ra] >>= regs[rb]; break;
        case SHL_R: regs[ra] <<= regs[rb]; break;
        case PH: regs[rb] -= 2; regs[ra] = getw(ibank|regs[rb]); break;
        case PL: setw(obank|regs[rb], regs[ra]); regs[rb] += 2; break;

        case L_W: regs[ra] = a; break;
        case JALR: regs[ra] = pc; pc = a; break;
        case ADD_W: regs[ra] = regs[rb] + a; break;
        case AND_W: regs[ra] = regs[rb] & a; break;
        case OR_W: regs[ra] = regs[rb] | a; break;
        case XOR_W: regs[ra] = regs[rb] ^ a; break;
        case LW_W: regs[ra] = getw(ibank|a); break;
        case LB_W: regs[ra] = memory[ibank|a]; break;
        case SW_W: setw(obank|a, regs[ra]); break;
        case SB_W: memory[obank|a] = regs[ra]; break;

        case L_B: regs[ra] = a; break;
        case ADD_B: regs[ra] = regs[rb] + a; break;
        case BZ: if(!regs[ra]) pc += a; break;
        case BNZ: if(regs[ra]) pc += a; break;
        case BM: if(regs[ra]<0) pc += a; break;
        case BNM: if(regs[ra]>=0) pc += a; break;
        case BEQ: if(regs[ra]==regs[rb]) pc += a; break;
        case BNE: if(regs[ra]!=regs[rb]) pc += a; break;
        case BLT: if(regs[ra]< regs[rb]) pc += a; break;
        case BLE: if(regs[ra]<=regs[rb]) pc += a; break;

        case JAL: regs[31] = pc; pc = b; break;
        case J: pc = b; break;

        default:
            switch(ins&0xf0) {
            case 0x00:
            case 0x10:
                pc -= 2;
                break;
            case 0x20:
                pc -= 4;
                break;
            case 0x30:
                pc -= 3;
                break;
            }

            flushLog();
            console.log("unknown instruction "+ins+" at "+pc);
            clearInterval(stepInterval);
            clearInterval(drawInterval);
            return;
        }

        pc &= 0xffff;
    }
}

function load(arr) {
    for(let i = 0; i < arr.length; i++)
        memory[ENTRY_POINT+i] = arr[i];
    pc = ENTRY_POINT;
    ibank = 0;
    obank = 0;
}

function draw() {
    let ts = getw(0);
    let s = cvs.width/320;

    for(let i = 0; i < 40*25; i++) {
        let t = memory[3+i];
        let c1 = memory[1003+i]&0xf;
        let c2 = memory[1003+i]>>4;

        for(let y = 0; y < 8; y++) {
            let e = memory[ts+t*8+y];
            for(let j = 0; j < 8; j++) {
                ctx.fillStyle = palette[(e&(1<<j)) ? c1 : c2];
                ctx.fillRect((i%40)*8*s+j*s,
                     Math.floor(i/40)*8*s+y*s,
                     s, s);
            }
        }
    }
}

let ctrl = false, shift = false;

addEventListener("keydown", function(e) {
    switch(e.keyCode) {
    case 16: shift = true; break;
    case 17: ctrl = true; break;
    case 9: memory[2] = shift ? 10 : 9; break;
    case 38: memory[2] = 16; break;
    case 40: memory[2] = 14; break;
    case 37: memory[2] = 2; break;
    case 39: memory[2] = 6; break;
    case 13: case 27:
        memory[2] = e.keyCode; break;
    default:
        if(ctrl && e.keyCode >= "A".charCodeAt(0) && e.keyCode <= "Z".charCodeAt(0))
            memory[2] = e.keyCode-"A".charCodeAt(0)+1;
        else if(e.key.length == 1)
            memory[2] = e.key.charCodeAt(0);
        break;
    }
});

addEventListener("keyup", function(e) {
    if(e.keyCode == 16) shift = false;
    if(e.keyCode == 17) ctrl = false;
});

load(file("fontest.bin"));

let stepInterval = setInterval(steps, 0);
let drawInterval = setInterval(draw, 150);

