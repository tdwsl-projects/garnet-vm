
; syscalls
:sys_quit   =0x00 ; exit the program
:sys_putc   =0x01 ; write char to stdout
:sys_getc   =0x02 ; read char from stdin
:sys_fopen  =0x03 ; open file, $0 = filename str, $1 = file mode
                  ;  returns file no. in $0, 0 on failure
:sys_fclose =0x04 ; close file, $0 = file no.
:sys_fputc  =0x05 ; write char to file, $0 = file no., $1 = char
:sys_fgetc  =0x06 ; read char from file, $0 = file no., $1 = char output
                  ; (-1 for eof)
:sys_feof   =0x07 ; check eof of file, $0 = file, $1 = output boolean -1/0
:sys_argc   =0x10 ; argc into $0
:sys_arg    =0x11 ; copy arg string, $0=arg $1=dest addr $2=max len

; file modes
:file_r =0
:file_w =1

; addresses
:mem_tiles  =0x0000 ; pointer to address of tileset
:mem_key    =0x0002 ; ascii value of last key pressed -
                    ; exceptions are as follows:
                    ; ctrl+(a-z) = 1-26
                    ; tab=9, backtab=10, return=13, backspace=127
                    ; up,down,left,right = 16,14,2,6 (ctrl+p,n,b,f)
:mem_sram   =0x0003 ; screen tiles from 0x0003-0x03ea
:mem_cram   =0x03eb ; colour memory from 0x03eb-0x07d2

