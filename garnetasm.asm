; garnetasm implemented natively

  *0x1000

  /syscalls.inc

:org  =26
:size =27

  l.w $30 #0x1000
  xor $org $org
  xor $size $size
  l.w $0 #symbolmem
  sw.w $0 #symbolmemp
  l.w $0 #symbols
  sw.w $0 #symbolp

  sys sys_argc
  l.b $1 3
  bne $0 $1 @noargs

  l.w $1 #filebuf
  l.w $2 #filebuf_sz
  l.b $0 1
  sys sys_arg
  jal #pass1

  l.w $1 #filebuf
  jal #pass2

  l.w $1 #buf
  l.w $2 #buf_sz
  l.b $0 2
  sys sys_arg
  j #donezo

:noargs
  l.w $1 #pr1
  jal #puts
  l.w $1 #filebuf
  jal #input

  l.w $1 #filebuf
  jal #pass1
  xor $org $org
  l.w $1 #filebuf
  jal #pass2

  l.w $1 #pr2
  jal #puts
  l.w $1 #buf
  jal #input

:donezo
  l.w $0 #buf
  jal #save

  sys sys_quit

:pr1 "Enter 32 "input 32 "filename: 10 0
:pr2 "Enter 32 "output 32 "filename: 10 0

:save
  ph $31 $30

  l $3 $0
  l.b $1 file_w
  sys sys_fopen
  bnz $0 @save_0

  j #fileerr

:save_0
  l.b $2 0
  ib 1

:save_1
  lb $1 $2
  sys sys_fputc
  inc $2
  bne $2 $size @save_1

  ib 0
  sys sys_fclose
  pl $31 $30
  jr $31

:pass2
  ; stack=$30 = :return addr, str, line no.:
  add.b $30 -2
  sw $29 $30
  l $29 $30
  add.b $30 -44
  sw $31 $30
  add.b $2 $30 2
  jal #strcpy
  add.b $1 $29 -2
  l.b $0 1
  sw $0 $1
  xor $0 $0
  add.b $1 $29 -5
  sb $0 $1

  add.b $0 $30 2
  l $3 $0
  l.b $1 file_r
  sys sys_fopen
  add.b $1 $29 -4
  sw $0 $1
  bnz $0 @pass2_0

  j #fileerr

:pass2_0
  l.w $2 #buf
  jal #parse
  l $4 $1

  l.w $1 #buf
  lb $2 $1

  bnz $2 @pass2_nz
  j #pass2_2

:pass2_nz

  l.b $3 ";
  bne $2 $3 @pass2_ncom

  jalr $28 #comment
  j #pass2_2

:pass2_ncom

  l.b $3 "/
  bne $2 $3 @pass2_ninc

  l.w $1 #buf
  inc $1
  jal #pass2
  add.b $0 $29 -4
  lw $0 $0
  j #pass2_2

:pass2_ninc

  l.b $3 "%
  bne $2 $3 @pass2_nincb

  l $8 $0
  add.b $0 $1 1
  l $3 $0
  l.b $1 file_r
  sys sys_fopen
  ob 1
  bnz $0 @pass2_incb_0

  j #fileerr

:pass2_incb_0
  ;sys sys_feof
  ;bnz $1 @pass2_incb_1
  sys sys_fgetc
  ;beq $1 $2 @pass2_incb_1
  bm $1 @pass2_incb_1
  sb $1 $size
  inc $org
  inc $size
  j #pass2_incb_0

:pass2_incb_1
  ob 0
  sys sys_fclose
  l $0 $8

  j #pass2_1

:pass2_nincb

  l.b $3 "*
  bne $2 $3 @pass2_norg

  inc $1
  jal #val
  l $org $2
  j #pass2_1

:pass2_norg

  l.b $3 ":
  bne $2 $3 @pass2_nlbl
  j #pass2_1
:pass2_nlbl
  l.b $3 "=
  bne $2 $3 @pass2_nequ
  j #pass2_1
:pass2_nequ
  l.b $3 "+
  bne $2 $3 @pass2_npls
  j #pass2_1
:pass2_npls

  l.b $3 ""
  bne $2 $3 @pass2_nquo

  l $5 $0
  inc $1
  l $2 $size
  ob 1
  jal #strcpy
  dec $2
  ob 0
  l $1 $2
  sub $1 $size
  add $org $1
  l $size $2
  l $0 $5

  j #pass2_1

:pass2_nquo

  l.b $3 "$
  bne $2 $3 @pass2_ndlr

  l $8 $2
  inc $1
  jal #val
  add.b $5 $29 -5
  lb $3 $5
  ib 1
  ob 1
  bne $8 $3 @pass2_dlr_0

  xor $3 $3
  sb $3 $5
  j #pass2_dlr_1

:pass2_dlr_0

  add.b $3 $size -1
  lb $5 $3
  l $6 $2
  shl $6 $3
  and.w $6 #0x00c0
  or $5 $6
  sb $5 $3

  l $5 $2
  shl $5 $5
  sb $5 $size

  inc $size
  inc $org

:pass2_dlr_1
  add.b $5 $size -1
  lb $3 $5
  and.w $3 #0x00e0
  and.w $2 #0x001f
  or $2 $3
  sb $2 $5
  ib 0
  ob 0
  j #pass2_1

:pass2_ndlr

  l.b $3 "#
  bne $2 $3 @pass2_nw

  inc $1
  jal #val
  ob 1
  sw $2 $size
  ob 0
  add.b $size 2
  add.b $org 2
  j #pass2_1

:pass2_nw

  l.b $3 "@
  bne $2 $3 @pass2_nrel

  inc $1
  jal #val
  inc $org
  sub $2 $org
  ob 1
  sb $2 $size
  ob 0
  inc $size
  j #pass2_1

:pass2_nrel

  jal #val
  ob 1
  sb $2 $size
  ob 0
  inc $size
  inc $org

:pass2_1
  l.w $1 #buf
  lb $2 $1
  add.b $3 $29 -5
  sb $2 $3

  l.b $3 10
  bne $4 $3 @pass2_nnl

  add.b $3 $29 -2
  lw $1 $3
  inc $1
  sw $1 $3

:pass2_nnl
:pass2_2
  sys sys_feof
  bnz $1 @pass2_3
  j #pass2_0

:pass2_3
  sys sys_fclose

  lw $31 $30
  l $30 $29
  lw $29 $30
  add.b $30 2
  jr $31

:pass1
  ; stack=$30 = :return addr, str, line no.:
  add.b $30 -2
  sw $29 $30
  l $29 $30
  add.b $30 -44
  sw $31 $30
  add.b $2 $30 2
  jal #strcpy
  add.b $1 $29 -2
  l.b $0 1
  sw $0 $1
  xor $0 $0
  add.b $1 $29 -5
  sb $0 $1

  l $1 $org
  l.b $4 4
  jal #puth
  l.b $0 32
  sys sys_putc

  add.b $0 $30 2
  l $3 $0
  l.b $1 file_r
  sys sys_fopen
  add.b $1 $29 -4
  sw $0 $1
  bnz $0 @pass1_0

  j #fileerr

:pass1_0
  l.w $2 #buf
  jal #parse
  l $4 $1

  l.w $1 #buf
  lb $2 $1

  bnz $2 @pass1_nz
  j #pass1_1

:pass1_nz

  l.b $3 ";
  bne $2 $3 @pass1_ncom

  jalr $28 #comment

  l $6 $0
  l $0 $5
  sys sys_putc
  l $1 $org
  l.b $4 4
  jal #puth
  l.b $0 32
  sys sys_putc
  l $0 $6
  l $4 $5

  j #pass1_2

:pass1_ncom

  l.b $3 "/
  bne $2 $3 @pass1_ninc

  jal #puts
  l.b $0 10
  sys sys_putc

  l.w $1 #buf
  inc $1
  jal #pass1
  add.b $0 $29 -4
  lw $0 $0
  j #pass1_2

:pass1_ninc

  l.b $3 "%
  bne $2 $3 @pass1_nincb

  l $8 $0
  add.b $0 $1 1
  l $3 $0
  ;l.w $2 #0x00ff
  l.b $1 file_r
  sys sys_fopen
  bnz $0 @pass1_incb_0

  j #fileerr

:pass1_incb_0
  ;sys sys_feof
  ;bnz $1 @pass1_incb_1
  sys sys_fgetc
  ;beq $1 $2 @pass1_incb_1
  bm $1 @pass1_incb_1
  inc $org
  j #pass1_incb_0

:pass1_incb_1
  sys sys_fclose
  l $0 $8

  j #pass1_1

:pass1_nincb

  l.b $3 "*
  bne $2 $3 @pass1_norg

  inc $1
  jal #val
  l $org $2
  j #pass1_1

:pass1_norg

  l.b $3 "#
  bne $2 $3 @pass1_nw

  add.b $org 2
  j #pass1_1

:pass1_nw

  l.b $3 ""
  bne $2 $3 @pass1_nquo

  inc $1
  l $3 $0
  jal #strlen
  l $0 $3
  add $org $2
  j #pass1_1

:pass1_nquo

  l.b $3 ":
  bne $2 $3 @pass1_nlbl

  inc $1
  jal #addsymbol
  add.b $0 $29 -4
  lw $0 $0
  j #pass1_1

:pass1_nlbl

  l.b $3 "=
  bne $2 $3 @pass1_nequ

  inc $1
  jal #val
  lw.w $3 #symbolp
  add.b $3 -2
  sw $2 $3
  j #pass1_1

:pass1_nequ

  l.b $3 "+
  bne $2 $3 @pass1_npls

  inc $1
  jal #val
  lw.w $3 #symbolp
  add.b $3 -2
  lw $5 $3
  add $2 $5
  sw $2 $3
  j #pass1_1

:pass1_npls

  l.b $3 "$
  bne $2 $3 @pass1_ndlr

  add.b $1 $29 -5
  lb $3 $1
  beq $2 $3 @pass1_dlr_0

  xor $3 $3
  sb $3 $1
  inc $org

:pass1_dlr_0

  j #pass1_1

:pass1_ndlr

  ; no prefix / @ prefix

  inc $org

:pass1_1

  ; log input

  l.w $1 #buf

  lb $2 $1
  add.b $3 $29 -5
  sb $2 $3

  add.b $30 -2
  sw $0 $30
  jal #puts
  l.b $0 32
  sys sys_putc

  l.b $0 10
  bne $4 $0 @pass1_nnl

  sys sys_putc

  add.b $2 $29 -2
  lw $1 $2
  inc $1
  sw $1 $2

  l $1 $org
  l.b $4 4
  jal #puth
  l.b $0 32
  sys sys_putc

:pass1_nnl
  lw $0 $30
  add.b $30 2

:pass1_2
  sys sys_feof
  bnz $1 @pass1_3
  j #pass1_0

:pass1_3

  sys sys_fclose

  l.b $0 10
  sys sys_putc

  lw $31 $30
  l $30 $29
  lw $29 $30
  add.b $30 2
  jr $31

:comment ; jalr w/ 28
  l.b $5 10
  beq $4 $5 @comment_1

:comment_0
  sys sys_feof
  bnz $1 @comment_1
  l.w $2 #buf
  jal #parse
  bne $1 $5 @comment_0

:comment_1
  add.b $2 $29 -2
  lw $1 $2
  inc $1
  sw $1 $2

  jr $28

:addsymbol ; $1=symbolstr
  ph $31 $30

  lw.w $0 #symbolp
  lw.w $2 #symbolmemp
  sw $2 $0
  add.b $0 2
  sw $org $0
  add.b $0 2
  sw.w $0 #symbolp

  jal #strcpy
  sw.w $2 #symbolmemp

  pl $31 $30
  jr $31

:puth ; $1 = n, $4 = no. of digits
  l.w $5 #nbuf
  add $5 $4
  xor $0 $0
  sb $0 $5

:puth_0
  and.w $0 $1 #0xf
  l.b $2 10
  blt $0 $2 @puth_1

  add.b $0 87 ; "a = 97
  j #puth_2

:puth_1
  add.b $0 "0

:puth_2
  dec $5
  sb $0 $5

  shr $1 $4
  dec $4
  bnz $4 @puth_0

  l.w $1 #nbuf
  l $4 $31
  jal #puts

  jr $4

:val ; value -> $2
  add.b $2 $30 -2
  sw $31 $2
  add.b $2 $30 -4
  sw $1 $2
  add.b $2 $30 -6
  sw $0 $2
  l $7 $4

  l $4 $1
  ;j #val_s_e

  ; find symbol

  l.w $5 #symbols
  lw.w $6 #symbolp
:val_s_0
  beq $5 $6 @val_s_e
  l $2 $4
  lw $3 $5
  jal #streq
  bne $0 $1 @val_s_1

  add.b $5 2
  lw $2 $5
  j #val_e

:val_s_1
  add.b $5 4
  j #val_s_0
:val_s_e

  ; find instruction

  l.w $5 #instructions
  xor $6 $6
:val_i_0
  lb $2 $5
  l.w $3 #0x00ff
  beq $2 $3 @val_i_e
  l $2 $4
  l $3 $5
  jal #streq
  bne $0 $1 @val_i_1

  l $2 $6
  j #val_e

:val_i_1
  l $1 $5
  jal #strend
  add.b $5 $1 1
  inc $6
  j #val_i_0

:val_i_e

  l $1 $4

  jal #number
  bz $0 @val_err

:val_e
  l $4 $7
  add.b $0 $30 -2
  lw $31 $0
  add.b $0 $30 -6
  lw $0 $0
  jr $31

:val_err
  l.w $1 #val_errmsgp1
  jal #puts
  add.b $1 $29 -2
  lw $1 $1
  jal #putn
  l.w $1 #val_errmsgp2
  jal #puts
  add.b $1 $30 2
  jal #puts
  l.w $1 #val_errmsgp3
  jal #puts
  add.b $1 $30 -4
  lw $1 $1
  jal #puts
  l.b $0 10
  sys sys_putc
  sys sys_quit

:val_errmsgp1 "value 32 "error 32 "on 32 "line 32 0
:val_errmsgp2 32 "in 32 0
:val_errmsgp3 ": 32 0

:streq
  lb $0 $2
  lb $1 $3
  bne $0 $1 @streq_0
  bz $0 @streq_0
  inc $2
  inc $3
  j #streq
:streq_0
  jr $31

:number
  xor $3 $3
  xor $2 $2
  lb $0 $1
  l.b $4 "-
  bne $0 $4 @number_0

  inc $3
  inc $1

:number_0
  lb $0 $1
  bnz $0 @number_1

  xor $0 $0
  jr $31

:number_1
  l.b $4 "0
  bne $0 $4 @number_nh

  inc $1
  lb $0 $1
  bnz $0 @number_nz

  inc $0
  jr $31

:number_nz

  l.b $4 "b
  bne $0 $4 @number_nb

:number_b_0
  inc $1
  lb $0 $1
  bnz $0 @number_b_1

  j #number_e

:number_b_1

  shl $2 $1

  l.b $4 "0
  beq $0 $4 @number_b_0
  l.b $4 "1
  bne $0 $4 @number_b_f
  inc $2
  j #number_b_0

:number_b_f
  xor $0 $0
  jr $31

:number_nb
  l.b $4 "x
  bne $0 $4 @number_nh

:number_h_0
  inc $1
  lb $0 $1
  bnz $0 @number_h_1

  j #number_e

:number_h_1

  l.b $4 "0
  blt $0 $4 @number_h_nd
  l.b $4 "9
  blt $4 $0 @number_h_nd

  add.b $0 -48 ; "0 = 48
  l.b $4 16
  mul $2 $4
  add $2 $0
  j #number_h_0

:number_h_nd
  l.b $4 "a
  blt $0 $4 @number_f
  l.b $4 "f
  blt $4 $0 @number_f

  add.b $0 -87 ; "a = 97
  l.b $4 16
  mul $2 $4
  add $2 $0
  j #number_h_0

:number_f
  xor $0 $0
  jr $31

:number_d_0
  lb $0 $1
  bz $0 @number_e

:number_nh
  l.b $4 "0
  blt $0 $4 @number_f
  l.b $4 "9
  blt $4 $0 @number_f

  add.b $0 -48 ; "0 = 48
  l.b $4 10
  mul $2 $4
  add $2 $0
  inc $1
  j #number_d_0

:number_e
  bz $3 @number_e_0
  xor.w $2 #0xffff
  inc $2

:number_e_0
  inc $0
  jr $31

:parse
  l.b $3 33
  l $4 $2

:parse_0
  sys sys_feof
  bnz $1 @parse_2
  sys sys_fgetc
  blt $1 $3 @parse_1

  sb $1 $2
  inc $2
  j #parse_0

:parse_1
  l.b $3 10
  beq $1 $3 @parse_2

  beq $2 $4 @parse

:parse_2
  xor $3 $3
  sb $3 $2
  jr $31

:fileerr
  l.w $1 #fileerr_msg
  jal #puts
  l $1 $3
  jal #puts
  l.b $0 10
  sys sys_putc
  sys sys_quit

:fileerr_msg "failed 32 "to 32 "open 32 0

:strend
  lb $0 $1
  bz $0 @strend_0
  inc $1
  j #strend
:strend_0
  jr $31

:strlen ; $1=str $2->len
  xor $2 $2
:strlen_0
  lb $0 $1
  bz $0 @strlen_1
  inc $1
  inc $2
  j #strlen_0
:strlen_1
  jr $31

:strcpy ; $1 -> $2
  lb $0 $1
  sb $0 $2
  bz $0 @strcpy_0
  inc $1
  inc $2
  j #strcpy
:strcpy_0
  inc $2
  jr $31

:putn
  l.w $2 #nbuf_end
  l $3 $2
  bnm $1 @putn_0

  xor.w $1 #0xffff
  inc $1
  l.b $0 "-
  sys sys_putc

:putn_0
  l.b $0 10
  div $1 $0
  dec $2
  sb $0 $2
  bnz $1 @putn_0

:putn_1
  lb $0 $2
  inc $2
  add.b $0 "0
  sys sys_putc
  bne $2 $3 @putn_1

  jr $31

:puts
  lb $0 $1
  bz $0 @puts_0
  sys sys_putc
  inc $1
  j #puts
:puts_0
  jr $31

:input
  sys sys_getc
  bz $0 @input_0
  add.b $2 $0 -10
  bz $2 @input_0

  sb $0 $1
  inc $1
  j #input

:input_0
  sb $2 $1
  jr $31

:instructions
"sys 0 "jr 0 "l 0 "lw 0 "lb 0 "sw 0 "sb 0 "add 0
"sub 0 "and 0 "or 0 "xor 0 "inc 0 "dec 0 "shr 0 "shl 0
"mul 0 "div 0 "ib 0 "ob 0 "shr.r 0 "shl.r 0 "ph 0 "pl 0  0 0 0 0  0 0 0 0
"l.w 0 "jalr 0 "add.w 0 "and.w 0 "or.w 0 "xor.w 0 "lw.w 0 "lb.w 0
"sw.w 0 "sb.w 0  0 0  0 0 0 0
"l.b 0 "add.b 0  0 0  "bz 0 "bnz 0 "bm 0 "bnm 0
"beq 0 "bne 0 "blt 0 "ble 0  0 0 "jal 0 "j 0
0xff

:buf
:buf_sz =120
:filebuf =buf +buf_sz
:filebuf_sz =120
:nbuf =filebuf +filebuf_sz
:nbuf_end =nbuf +40
:symbolp =nbuf_end
:symbols =symbolp +2
:symbolmem =symbols +800
:symbolmemp =symbolmem +2048
